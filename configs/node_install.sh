#!/bin/bash -e
# Install exporter for prometeus
if [[ ! -f "$(ls . | grep node_exporter-*.tar.gz)" ]]; 
then
    wget https://github.com/prometheus/node_exporter/releases/download/v1.5.0/node_exporter-1.5.0.linux-amd64.tar.gz
fi 

tar xvfz node_exporter-*.tar.gz

sudo mv node_exporter-1.5.0.linux-amd64/node_exporter /usr/local/bin 
sudo useradd -rs /bin/false node_exporter  
sudo mv node_exporter.service /etc/systemd/system/node_exporter.service  
sudo systemctl enable node_exporter  
sudo systemctl daemon-reload  
sudo systemctl start node_exporter  
sudo ufw allow 9100
# rm -r node_exporter-1.5.0.linux-amd64*
