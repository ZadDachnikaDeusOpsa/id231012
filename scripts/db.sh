#!/bin/bash -e

export DEBIAN_FRONTEND=noninteractive

sudo apt update && \
     apt install -y mysql-server
#configure MYSQL
sudo sed -i s/127.0.0.1/0.0.0.0/g /etc/mysql/mysql.conf.d/mysqld.cnf
sudo mysql --execute="CREATE USER 'pma'@'%' IDENTIFIED BY 'pma'; GRANT ALL PRIVILEGES ON *.* TO 'pma'@'%'; FLUSH PRIVILEGES;"
sudo systemctl restart mysql.service

# Install exporter for prometeus
if [[  -f "$(ls . | grep node_install.sh)" ]]; 
then
    sudo chmod +x node_install.sh
    ./node_install.sh
fi
