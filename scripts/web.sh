#!/bin/bash -e

export DEBIAN_FRONTEND=noninteractive

sudo apt update && \
     apt install -y phpmyadmin nginx unzip && \
     apt purge -y *apache2*

# NGINX
PHP_VER=`php -v | head -n 1 | cut -d " " -f2 | cut -b 1-3`
sudo rm -r /etc/nginx/sites-enabled/* && \
    mv phpmyadmin.conf /etc/nginx/sites-available/ && \
    sed -i s/\$PHP_VER/$PHP_VER/g /etc/nginx/sites-available/phpmyadmin.conf && \
    ln -s /etc/nginx/sites-available/phpmyadmin.conf /etc/nginx/sites-enabled/phpmyadmin.conf && \
    chown -R www-data:www-data /usr/share/phpmyadmin && \
    systemctl restart nginx && \
    systemctl restart php$PHP_VER-fpm
    
# config PhpMyAdmin
sudo cat /etc/hosts | grep db | \
while read -r line
    do hst=`echo $line | cut -d " " -f2`
        sudo cat <<PARAM >> /etc/phpmyadmin/config.inc.php 
/* Параметры сервера */
\$cfg['Servers'][\$i]['host'] = '$hst';
\$cfg['Servers'][\$i]['connect_type'] = 'tcp';
\$cfg['Servers'][\$i]['compress'] = false;
/* Select mysqli if your server has it */
\$cfg['Servers'][\$i]['extension'] = 'mysql';
\$cfg['Servers'][\$i]['AllowNoPassword'] = false;

PARAM
done

# Install exporter for prometeus
if [[  -f "$(ls . | grep node_install.sh)" ]]; 
then
    sudo chmod +x node_install.sh
    ./node_install.sh
fi 

# Install App - Piwigo
if [[  -f "$(ls . | grep piwigo-*.zip)" ]]; 
then
    curl https://piwigo.org/download/dlcounter.php?code=latest --output piwigo-latest.zip
fi 

unzip -q piwigo-*.zip
sudo mkdir -p /var/www/photos
sudo mv piwigo/* /var/www/photos/
sudo chown www-data: -R /var/www/photos/
sudo mv piwigo.conf /etc/nginx/sites-available/ && \
    sed -i s/\$PHP_VER/$PHP_VER/g /etc/nginx/sites-available/piwigo.conf && \
    ln -s /etc/nginx/sites-available/piwigo.conf /etc/nginx/sites-enabled/
sudo systemctl restart nginx
