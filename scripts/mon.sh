#!/bin/bash -e

export DEBIAN_FRONTEND=noninteractive

sudo apt update
# Install Prometheus
# https://www.linode.com/docs/guides/how-to-install-prometheus-and-grafana-on-ubuntu/
if [[ ! -f "$(ls . | grep prometheus-*.tar.gz)" ]]; 
then
    wget https://github.com/prometheus/prometheus/releases/download/v2.48.1/prometheus-2.48.1.linux-amd64.tar.gz
fi

tar xvfz prometheus-*.tar.gz

sudo mkdir /etc/prometheus /var/lib/prometheus

cd prometheus-2.48*-amd64

sudo chmod +x prometheus promtool

sudo mv prometheus promtool /usr/local/bin/

sudo mv prometheus.yml /etc/prometheus/prometheus.yml

sudo mv consoles/ console_libraries/ /etc/prometheus/

sudo useradd -rs /bin/false prometheus

sudo chown -R prometheus: /etc/prometheus /var/lib/prometheus

sudo cat <<PARAM >> /etc/systemd/system/prometheus.service
[Unit] 
Description=Prometheus 
Wants=network-online.target 
After=network-online.target 

[Service] 
User=prometheus 
Group=prometheus 
Type=simple 
Restart=on-failure 
RestartSec=5s 
ExecStart=/usr/local/bin/prometheus \
--config.file /etc/prometheus/prometheus.yml \
--storage.tsdb.path /var/lib/prometheus/ \
--web.console.templates=/etc/prometheus/consoles \
--web.console.libraries=/etc/prometheus/console_libraries \
--web.listen-address=0.0.0.0:9090 \
--web.enable-lifecycle \
--log.level=info 

[Install] 
WantedBy=multi-user.target
PARAM

tail -n $(($(wc -l < /etc/hosts) - $(awk '/'"$(cat /etc/hostname)"'/ {print NR; exit}' /etc/hosts))) /etc/hosts | \
while read -r line 
do
    hstnm=`echo $line | cut -d " " -f2`
    hstip=`echo $line | cut -d " " -f1`
    if [[ "$(cat /etc/hostname)" != "$hstnm" ]]; 
    then 
        sudo cat <<PARAM >> /etc/prometheus/prometheus.yml
  - job_name: "$hstnm"
    static_configs:
      - targets: ["$hstip:9100"]
PARAM
fi
done

sudo systemctl daemon-reload

sudo systemctl enable prometheus

sudo systemctl start prometheus
##
cd ~/
echo `pwd`
# Install Graphana
sudo apt-get install -y apt-transport-https software-properties-common

sudo mkdir -p /etc/apt/keyrings/
wget -q -O - https://apt.grafana.com/gpg.key | gpg --dearmor | sudo tee /etc/apt/keyrings/grafana.gpg > /dev/null

echo "deb [signed-by=/etc/apt/keyrings/grafana.gpg] https://apt.grafana.com stable main" | sudo tee -a /etc/apt/sources.list.d/grafana.list

echo "deb [signed-by=/etc/apt/keyrings/grafana.gpg] https://apt.grafana.com beta main" | sudo tee -a /etc/apt/sources.list.d/grafana.list

sudo apt-get update

sudo apt-get install grafana

# sudo apt install -y adduser libfontconfig1 musl
# if [[ ! -f "$(ls . | grep grafana_*.deb)" ]]; 
# then
#     wget https://dl.grafana.com/oss/release/grafana_10.2.2_amd64.deb
# fi
# sudo apt install ./grafana_10.2.2_amd64.deb
